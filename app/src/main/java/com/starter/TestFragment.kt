package com.starter

import android.os.Bundle
import com.starter.di.Bar
import com.starter.di.Example2Module
import com.starter.di.ExampleModule
import com.starter.di.Foo
import javax.inject.Inject

class TestFragment : InjectableFragment({ arrayOf(ExampleModule(it), Example2Module()) }) {

    companion object {
        fun newInstance() = TestFragment()
    }

    @Inject lateinit var foo: Foo
    @Inject lateinit var bar: Bar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        println("Foo=$foo, bar=$bar")
    }
}