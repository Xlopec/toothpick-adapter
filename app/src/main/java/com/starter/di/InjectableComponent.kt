package com.foodinhoods.infrastructure.di

/**
 *
 *
 * Represent dagger component which can be inject
 * into specific view. You don't need to implement this
 * interface manually, Dagger does it for you
 *
 *
 * Created by Максим on 2/16/2017.
 *
 * @param <Into> non abstract class (Dagger injection limitation) whose fields
 * needs to be injected
</Into> */

interface InjectableComponent<Into> {

    fun inject(into: Into)

}
