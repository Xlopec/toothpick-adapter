package com.starter.di

import android.app.Activity
import android.app.FragmentManager
import android.app.LoaderManager
import android.view.LayoutInflater
import toothpick.config.Module
import toothpick.smoothie.provider.FragmentManagerProvider
import toothpick.smoothie.provider.LayoutInflaterProvider
import toothpick.smoothie.provider.LoaderManagerProvider

class ActivityModule(activity: Activity) : Module() {
    init {
        this.bind(Activity::class.java).toInstance(activity)
        this.bind(FragmentManager::class.java).toProviderInstance(FragmentManagerProvider(activity))
        this.bind(LoaderManager::class.java).toProviderInstance(LoaderManagerProvider(activity))
        this.bind(LayoutInflater::class.java).toProviderInstance(LayoutInflaterProvider(activity))
    }
}