package com.starter.di;

import toothpick.config.Module;

public class Example2Module extends Module {

    public Example2Module() {
        bind(Bar.class);
    }

}
