package com.starter.di;

import javax.inject.Inject;

public class Foo {

    @Inject
    public Foo(String a) {
        System.out.println("WAS " + a);
    }

}
