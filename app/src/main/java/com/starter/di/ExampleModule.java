package com.starter.di;

import android.support.v4.app.Fragment;

import toothpick.config.Module;

public class ExampleModule extends Module {

    public ExampleModule(Fragment fragment) {
        bind(Fragment.class).toInstance(fragment);
        bind(Foo.class).singletonInScope();
        bind(String.class).toInstance("Allah");
    }

}
