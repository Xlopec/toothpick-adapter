package com.starter.di;

import javax.inject.Inject;

public class Bar {

    @Inject
    public Bar(Foo foo) {
        System.out.println("Foo " + foo);
    }

}
