package com.foodinhoods.infrastructure.di.scopes

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Scope

/**
 *
 *
 * One instance per activity
 *
 * Created by Максим on 10/11/2016.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class ActivityScope
