package com.foodinhoods.infrastructure.di.scopes

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Scope

/**
 *
 *
 * One instance per broadcast receiver
 *
 * Created by Максим on 11/7/2016.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class BroadcastReceiverScope
