package com.starter

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import com.starter.di.Key
import com.starter.di.util.generateKey
import toothpick.Toothpick
import toothpick.config.Module

abstract class InjectableFragment protected constructor(private inline val provider: (InjectableFragment) -> Array<Module>) : Fragment() {

    companion object {
        private val TAG = InjectableFragment::class.java.name!!
        private const val ARG_KEY = "argKey"
    }

    protected constructor(module: Module, vararg modules: Module) : this({ arrayOf(module, *modules) })

    protected constructor(module: Module) : this({ arrayOf(module) })

    private lateinit var key: Key
    private var isOnSaveStateCalled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        key = savedInstanceState?.getParcelable(ARG_KEY) ?: generateKey(this)

        requireActivity().let { Toothpick.openScopes(it.application, it, key, this) }
                .also { it.installModules(*provider(this)) }
                .also { Toothpick.inject(this, it) }

        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        try {
            if (isRemoving || !isOnSaveStateCalled) {
                // fragment won't be recreated later
                // destroy to avoid memory leaks, injections
                // which should exist across activity re-creation
                // should be configured in appropriate config module
                Toothpick.closeScope(key)
                Log.d(TAG, String.format("Components for %s were released", javaClass.name))
            }
        } finally {
            isOnSaveStateCalled = false
            super.onDestroy()
        }
    }
}