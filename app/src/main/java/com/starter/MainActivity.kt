package com.starter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    //@Inject lateinit var foo: Foo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

       /* val scope = Toothpick.openScopes(application, "allah", this)

        scope.installModules(SmoothieActivityModule(this))

        Toothpick.inject(this, scope)*/

        setContentView(R.layout.activity_main)

       // println("A foo $foo")

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, TestFragment.newInstance())
                .commit()
    }
}
